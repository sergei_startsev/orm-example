# ACHTUNG! #

This project is an example of using NHibernate (with Fluent NHibernate) from [here](https://github.com/jagregory/fluent-nhibernate/wiki/Getting-started) with author changes.

[Here](https://github.com/jagregory/fluent-nhibernate/wiki/Database-configuration) you can see examples of the configuration for other DB.